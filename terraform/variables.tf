

variable "region" {
  default     = "eu-west-3"
  description = "AWS region"
}


variable "cluster-name" {
  default = "eks-learning"
}


locals {
  project_tags = {
    usecase             = "learning"
    tutorial            = "k8s-aws"
    kubernettes-cluster = "${var.cluster-name}"
  }
}

variable "instance-ami" {
  default = "ami-0f7cd40eac2214b37"
}


variable "instance-type" {
  default = "t2.nano"
}

variable "key_path" {
  default = "~/.ssh/id_rsa.pub"
}


