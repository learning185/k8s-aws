resource "aws_security_group" "private_workers_ssh" {
  name_prefix = "private_workers_ssh"
  vpc_id      = module.vpc.vpc_id

  description = "security group that allows ssh from public networks and all egress traffic"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = module.vpc.public_subnets_cidr_blocks

  }
  tags = merge(
    local.project_tags,
    {
      Name = "sg-ssh-private-eks-${var.region}"
    }
  )
}

resource "aws_security_group" "public_workers_ssh" {
  name_prefix = "public_workers_ssh"
  vpc_id      = module.vpc.vpc_id

  description = "security group that allows ssh and all egress traffic"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    local.project_tags,
    {
      Name = "sg-ssh-public-eks-${var.region}"
    }
  )
}


resource "aws_security_group" "rds_k8s" {
  name_prefix = "rds_k8s"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = concat(
      module.vpc.public_subnets_cidr_blocks,
      module.vpc.private_subnets_cidr_blocks
    )
  }

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.project_tags,
    {
      Name = "sg-rds-eks-${var.region}"
    }
  )
}
