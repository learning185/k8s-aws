
data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}


module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.cluster-name
  cluster_version = "1.20"
  subnets         = module.vpc.private_subnets


  tags = merge(
    local.project_tags,
    {
      Name = "eks-cluster-${var.region}"
    }
  )
  vpc_id = module.vpc.vpc_id

  workers_group_defaults = {
    root_volume_type = "gp2"
    disk_size        = 50
  }

  worker_groups = [
    {
      name                          = "worker-group-default-eks"
      key_name = aws_key_pair.mykeypair.key_name
      instance_type                 = "t2.small"
      additional_userdata           = "echo foo bar"
      asg_min_size                  = 3
      asg_desired_capacity          = 3
      asg_max_size                  = 6
      additional_security_group_ids = [aws_security_group.private_workers_ssh.id]
    },
  ]

  depends_on = [
    module.vpc,
  ]
}
