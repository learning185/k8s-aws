resource "aws_instance" "bastion-instance" {
  ami           = var.instance-ami
  instance_type = var.instance-type

  subnet_id = module.vpc.public_subnets[0]

  vpc_security_group_ids = [aws_security_group.public_workers_ssh.id]

  key_name = aws_key_pair.mykeypair.key_name

  tags = merge(
    local.project_tags,
    {
      Name = "bastion-instance"
    }
  )
}

resource "aws_instance" "private-instance" {
  ami           = var.instance-ami
  instance_type = var.instance-type

  subnet_id = module.vpc.private_subnets[0]

  vpc_security_group_ids = [aws_security_group.private_workers_ssh.id]

  key_name = aws_key_pair.mykeypair.key_name

  tags = merge(
    local.project_tags,
    {
      Name = "private-instance"
    }
  )
}

resource "aws_key_pair" "mykeypair" {
  key_name   = "terraform"
  public_key = file(var.key_path)
}


