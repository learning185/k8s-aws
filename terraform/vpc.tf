

data "aws_availability_zones" "available" {}


module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.66.0"

  name               = "learning-vpc"
  cidr               = "10.0.0.0/16"
  azs                = ["${var.region}a", "${var.region}b", "${var.region}c"]
  private_subnets    = ["10.0.110.0/24", "10.0.120.0/24", "10.0.130.0/24"]
  public_subnets     = ["10.0.50.0/24", "10.0.60.0/24", "10.0.70.0/24"]
  enable_nat_gateway = true
  single_nat_gateway = true

  tags = merge(
    local.project_tags,
    {
      Name = "vpc-eks-${var.region}"
    }
  )

  public_subnet_tags = merge(
    local.project_tags,
    {
      Name = "public-network-eks"
    }
  )

  private_subnet_tags = merge(
    local.project_tags,
    {
      Name = "private-network-eks"
    }
  )


}

